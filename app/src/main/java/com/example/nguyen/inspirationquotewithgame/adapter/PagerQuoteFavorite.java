package com.example.nguyen.inspirationquotewithgame.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.nguyen.inspirationquotewithgame.R;
import com.example.nguyen.inspirationquotewithgame.model.InspirationQuote;

import java.util.List;

/**
 * Created by Administrator on 14/04/2018.
 */

public class PagerQuoteFavorite extends PagerAdapter {
    private List<InspirationQuote> mListQuote;
    private Context mContext;

    public PagerQuoteFavorite(List<InspirationQuote> mListQuote, Activity mContext) {
        this.mListQuote = mListQuote;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return mListQuote.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.pager_quote_favorite, container, false);
        container.addView(layout);

        TextView mTextviewQuote = layout.findViewById(R.id.pager_tv_quote);
        ImageView mImageShare = layout.findViewById(R.id.pager_image_share);

        final InspirationQuote inspirationQuote = mListQuote.get(position);
        mTextviewQuote.setText(inspirationQuote.getQuote()+"\n"+"-"+inspirationQuote.getAuthor()+"-");

        mImageShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareQuote(inspirationQuote.getQuote()+"\n-"+inspirationQuote.getAuthor()+"-");
            }
        });
        return layout;
    }

    private void shareQuote(String quoteAndAuthor){
        String shareBody = quoteAndAuthor+"\nCheck "+mContext.getString(R.string.app_name)+" in Google Play Store";
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Share Quote");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        mContext.startActivity(Intent.createChooser(sharingIntent, mContext.getString(R.string.share_using)));
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
}

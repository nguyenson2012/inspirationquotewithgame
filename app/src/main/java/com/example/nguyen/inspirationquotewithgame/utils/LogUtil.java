package com.example.nguyen.inspirationquotewithgame.utils;

import android.util.Log;

/**
 * Created by Administrator on 14/11/2017.
 */

public class LogUtil {
    public static void d(String tag,String message){
        Log.d(tag,message);
    }

    public static void e(String tag,String message){
        Log.e(tag,message);
    }
}

package com.example.nguyen.inspirationquotewithgame.fragment;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.example.nguyen.inspirationquotewithgame.R;
import com.example.nguyen.inspirationquotewithgame.database.DatabaseManager;
import com.example.nguyen.inspirationquotewithgame.database.QuoteInDatabase;
import com.example.nguyen.inspirationquotewithgame.model.InspirationQuote;
import com.example.nguyen.inspirationquotewithgame.utils.Constant;
import com.example.nguyen.inspirationquotewithgame.utils.MyPreferences;
import com.example.nguyen.inspirationquotewithgame.utils.ScreenManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by nguyen on 22/04/2018.
 */

public class FragmentPlay extends Fragment {
    private TextView mTextviewTimeLeft;
    private TextView mTextviewScore;
    private TextView mTextviewQuote;
    private EditText mEditextWordOne;
    private EditText mEdittextWordTwo;
    private Button mButtonCheck;

    private int mTotalScore = 0;
    private String missingWordOne = "";
    private String missingWordTwo = "";
    private CountDownTimer countDownTimer;
    private List<InspirationQuote> allQuote;
    public static final String TAG = "FragmentPlay";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_play_get_gift,container,false);
        setupView(rootView);
        return rootView;
    }

    private void setupView(View rootView) {
        mTextviewTimeLeft = rootView.findViewById(R.id.play_tv_time_countdown);
        mTextviewScore = rootView.findViewById(R.id.play_tv_score);
        mTextviewQuote = rootView.findViewById(R.id.play_textview_quote_missing);
        mEditextWordOne = rootView.findViewById(R.id.play_edittext_word_one);
        mEdittextWordTwo = rootView.findViewById(R.id.play_edittext_word_two);
        mButtonCheck = rootView.findViewById(R.id.play_button_check_answer);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initliaze();
        mButtonCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String wordOne=mEditextWordOne.getText().toString().replace(" ","").toLowerCase();
                String wordTwo=mEdittextWordTwo.getText().toString().replace(" ","").toLowerCase();
                String combineMissingWord = wordOne+wordTwo;
                String reserveCombineWord = wordTwo+wordOne;

                if(combineMissingWord.contains(missingWordOne.toLowerCase()+""+missingWordTwo.toLowerCase()) ||
                        reserveCombineWord.contains(missingWordOne.toLowerCase()+""+missingWordTwo.toLowerCase())){
                    mTotalScore+=10;
                    mTextviewScore.setText(""+mTotalScore);
                    mEditextWordOne.setText("");
                    mEdittextWordTwo.setText("");
                    showRandomQuote();
                }else {
                    Toast.makeText(getActivity(),getString(R.string.answe_was_wrong),Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void initliaze() {
        allQuote =new ArrayList<>();
        List<QuoteInDatabase> allQuoteInDatabase = DatabaseManager.getInstance().getAllQuote();
        for(QuoteInDatabase quoteInDatabase : allQuoteInDatabase){
            InspirationQuote inspirationQuote = new InspirationQuote(quoteInDatabase.getId(),quoteInDatabase.getQuote(),quoteInDatabase.getAuthor());
            allQuote.add(inspirationQuote);
        }
        //reset combine id of learned quote
        MyPreferences.getInstance(getActivity()).saveStringValue(Constant.KEY_SAVE_COMBINE_PLAYED_QUOTE_ID,"");
        showRandomQuote();
        countDownTimer = new CountDownTimer(Constant.TOTAL_TIME_FOR_CHALLENGE,Constant.TIME_INTERVAL) {
            @Override
            public void onTick(long millisUntilFinish) {
                int minutes = (int) (millisUntilFinish/1000/60);
                int seconds = (int) ((millisUntilFinish/1000)%60);
                mTextviewTimeLeft.setText(convertOneDigit2TwoDigit(minutes)+":"+convertOneDigit2TwoDigit(seconds));
            }

            @Override
            public void onFinish() {
                //show result
                int totalScoreInMonth = MyPreferences.getInstance(getActivity()).getIntValue(Constant.KEY_SAVE_MONTH_SCORE,0);
                MyPreferences.getInstance(getActivity()).saveIntValue(Constant.KEY_SAVE_MONTH_SCORE,totalScoreInMonth+mTotalScore);
                ScreenManager.getInst().openFragmentWithAnimation(getActivity().getSupportFragmentManager(),R.id.content_frame,new FragmentResult(),false,FragmentResult.TAG);
            }
        };
        countDownTimer.start();
    }

    private void showRandomQuote() {
        //1.get random quote
        if(allQuote.size() == 0){
            return;
        }
        int randomPosition = new Random().nextInt(allQuote.size());
        InspirationQuote inspirationQuote = allQuote.get(randomPosition);
        String combineLearnedQuoteID = MyPreferences.getInstance(getActivity()).getStringValue(Constant.KEY_SAVE_COMBINE_PLAYED_QUOTE_ID);
        MyPreferences.getInstance(getActivity()).saveStringValue(Constant.KEY_SAVE_COMBINE_PLAYED_QUOTE_ID,combineLearnedQuoteID+"-"+inspirationQuote.getId());
        allQuote.remove(randomPosition);
        //2.choose two random missing word
        String[] wordInQuotes = inspirationQuote.getQuote().split(" ");
        List<Integer> wordPositions = new ArrayList<>();
        do{
            int positionWord = new Random().nextInt(wordInQuotes.length);
            if(!wordPositions.contains(new Integer(positionWord)) && isStringOnly(wordInQuotes[positionWord])){
                wordPositions.add(new Integer(positionWord));
            }
        }while (wordPositions.size() != 2);
        missingWordOne = wordInQuotes[wordPositions.get(0)];
        missingWordTwo = wordInQuotes[wordPositions.get(1)];
        //3.display missing quote
        StringBuilder replaceQuote = new StringBuilder("");
        for( int i=0;i<wordInQuotes.length;i++){
            if(wordPositions.contains(new Integer(i))){
                replaceQuote.append(" ").append("____");
            }else {
                replaceQuote.append(" ").append(wordInQuotes[i]);
            }
        }
        replaceQuote.append("\n").append("-").append(inspirationQuote.getAuthor()).append("-");
        mTextviewQuote.setText(replaceQuote.toString());
    }

    private String convertOneDigit2TwoDigit(int number){
        if(number >= 10){
            return number + "";
        }else {
            return "0"+number;
        }
    }

    public boolean isStringOnly(String name) {
        return name.matches("[a-zA-Z]+");
    }
}

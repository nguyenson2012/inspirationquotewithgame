package com.example.nguyen.inspirationquotewithgame;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.example.nguyen.inspirationquotewithgame.fragment.FragmentFavorite;
import com.example.nguyen.inspirationquotewithgame.fragment.FragmentMain;
import com.example.nguyen.inspirationquotewithgame.fragment.FragmentNotification;
import com.example.nguyen.inspirationquotewithgame.fragment.FragmentOwnQuote;
import com.example.nguyen.inspirationquotewithgame.fragment.FragmentPlay;
import com.example.nguyen.inspirationquotewithgame.receiver.AlarmReceiver;
import com.example.nguyen.inspirationquotewithgame.utils.Constant;
import com.example.nguyen.inspirationquotewithgame.utils.MyPreferences;
import com.example.nguyen.inspirationquotewithgame.utils.OnClickDialogInterface;
import com.example.nguyen.inspirationquotewithgame.utils.ScreenManager;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;
    private ImageView mImageToogleDrawer;
    private TextView mTextviewToolbarTitle;

    private MyPreferences myPreferences;
    private AlarmManager mAlarmManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        showFragmentMain();
        mImageToogleDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawerLayout.openDrawer(GravityCompat.START);
            }
        });
    }

    private void showFragmentMain() {
        FragmentMain fragmentHome=new FragmentMain();
        ScreenManager.getInst().openFragmentWithAnimation(getSupportFragmentManager(),R.id.content_frame,fragmentHome,
                false,FragmentMain.TAG);
    }

    private void init() {
        myPreferences = MyPreferences.getInstance(MainActivity.this);
        mAlarmManager=(AlarmManager) getSystemService(Context.ALARM_SERVICE);
        mImageToogleDrawer = findViewById(R.id.main_image_toggle_open_drawer_layout);
        mTextviewToolbarTitle = findViewById(R.id.main_tv_toolbar_title);
        mDrawerLayout = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        menuItem.setChecked(true);
                        mDrawerLayout.closeDrawers();

                        switch (menuItem.getItemId()){
                            case R.id.nav_home:
                                openFragmentMainIfNecessary();
                                mTextviewToolbarTitle.setText(getString(R.string.be_awesome));
                                break;
                            case R.id.nav_own_quote:
                                openFragmentOwnQuote();
                                mTextviewToolbarTitle.setText(getString(R.string.add_own_quote_tittle));
                                break;
                            case R.id.nav_challenge:
                                openFragmentPlay();
                                mTextviewToolbarTitle.setText(getString(R.string.challenge));
                                break;
                            case R.id.nav_favorite:
                                String combineFavoriteQuoteID = myPreferences.getStringValue(Constant.KEY_SAVE_COMBINE_FAVORITE_QUOTE_ID);
                                if(combineFavoriteQuoteID.length() > 1){
                                    openFragmentFavorite();
                                    mTextviewToolbarTitle.setText(getString(R.string.favorite));
                                }else {
                                    Toast.makeText(MainActivity.this,getString(R.string.no_favorite_quote),Toast.LENGTH_SHORT).show();
                                }
                                break;
                            case R.id.nav_about_us:
                                showDialogAboutUs();
                                break;
                            case R.id.nav_rate_us:
                                rateApp();
                                break;
                            case R.id.nav_notification:
                                mTextviewToolbarTitle.setText(getString(R.string.notification));
                                openFragmentNotification();
                        }
                        return true;
                    }
                });
    }

    private void openFragmentPlay() {
        Fragment fragmentPlay = getSupportFragmentManager().findFragmentByTag(FragmentPlay.TAG);
        if(fragmentPlay == null || !fragmentPlay.isVisible()){
            Fragment fragmentMain = getSupportFragmentManager().findFragmentByTag(FragmentMain.TAG);
            if(fragmentMain == null || !fragmentMain.isVisible()){
                ScreenManager.getInst().openFragmentWithAnimation(getSupportFragmentManager(),R.id.content_frame,new FragmentPlay(),false,FragmentPlay.TAG);
            }else {
                ScreenManager.getInst().openFragmentWithAnimation(getSupportFragmentManager(),R.id.content_frame,new FragmentPlay(),true,FragmentPlay.TAG);

            }
        }else {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        }
    }

    private void openFragmentOwnQuote() {
        Fragment fragmentOwnQuote = getSupportFragmentManager().findFragmentByTag(FragmentOwnQuote.TAG);
        if(fragmentOwnQuote == null || !fragmentOwnQuote.isVisible()){
            Fragment fragmentMain = getSupportFragmentManager().findFragmentByTag(FragmentMain.TAG);
            if(fragmentMain == null || !fragmentMain.isVisible()){
                ScreenManager.getInst().openFragmentWithAnimation(getSupportFragmentManager(),R.id.content_frame,new FragmentOwnQuote(),false,FragmentOwnQuote.TAG);
            }else {
                ScreenManager.getInst().openFragmentWithAnimation(getSupportFragmentManager(),R.id.content_frame,new FragmentOwnQuote(),true,FragmentOwnQuote.TAG);

            }
        }else {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        }
    }

    private void openFragmentNotification() {
        Fragment fragmentFavorite = getSupportFragmentManager().findFragmentByTag(FragmentFavorite.TAG);
        if(fragmentFavorite == null || !fragmentFavorite.isVisible()){
            Fragment fragmentMain = getSupportFragmentManager().findFragmentByTag(FragmentMain.TAG);
            if(fragmentMain == null || !fragmentMain.isVisible()){
                ScreenManager.getInst().openFragmentWithAnimation(getSupportFragmentManager(),R.id.content_frame,new FragmentNotification(),false,FragmentNotification.TAG);
            }else {
                ScreenManager.getInst().openFragmentWithAnimation(getSupportFragmentManager(),R.id.content_frame,new FragmentNotification(),true,FragmentFavorite.TAG);

            }
        }else {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        }
    }

    @Override
    public void onBackPressed() {
        Fragment fragmentMain = getSupportFragmentManager().findFragmentByTag(FragmentMain.TAG);
        if(fragmentMain!=null){
            if(fragmentMain.isVisible()){
                ScreenManager.getInst().showDialog(MainActivity.this, "", getString(R.string.want_exit_app),
                        new OnClickDialogInterface() {
                            @Override
                            public void onClickOK() {
                                MainActivity.this.finish();
                            }

                            @Override
                            public void onClickCancel() {

                            }
                        });
            }else {
                super.onBackPressed();
                Fragment currentFragment = getSupportFragmentManager().findFragmentByTag(FragmentMain.TAG);
                if(currentFragment.isVisible()){
                    mTextviewToolbarTitle.setText(getString(R.string.be_awesome));
                }
            }
        }else {
            super.onBackPressed();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void showDialogAboutUs() {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(MainActivity.this)
                .title(getString(R.string.about_us))
                .content(getString(R.string.about_us_description))
                .positiveText(getString(R.string.go_fanpage))
                .negativeText(getString(R.string.send_feedback))
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        //go fanpage link
                        openFacebookGroupLink();
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        //send feedback
                        sendFeedback();
                    }
                });
        MaterialDialog dialog = builder.build();
        dialog.show();
    }

    private void rateApp() {
        myPreferences.saveBooleanValue(Constant.KEY_DONE_RATE_APP,true);
        try {
            Intent rateIntent = new Intent(Intent.ACTION_VIEW);
            rateIntent.setData(Uri.parse(getString(R.string.app_market_id)
                    + getPackageName()));
            startActivity(rateIntent);
        } catch (Exception e) {
            Toast.makeText(MainActivity.this,getString(R.string.error_happen),Toast.LENGTH_SHORT).show();
            Log.e("RateApp",e.toString()+"");
        }
    }

    private void openFacebookGroupLink() {
        String url = getString(R.string.facebook_group);
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }


    private void sendFeedback() {
        Intent Email = new Intent(Intent.ACTION_SEND);
        Email.setType("text/email");
        Email.putExtra(Intent.EXTRA_EMAIL, new String[] { getString(R.string.email_contact) });
        Email.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.feedback));
        Email.putExtra(Intent.EXTRA_TEXT, getString(R.string.feedback_header_email) + "");
        startActivity(Intent.createChooser(Email, getString(R.string.send_feedback)));
    }

    private void openFragmentMainIfNecessary() {
        Fragment fragmentMain = getSupportFragmentManager().findFragmentByTag(FragmentMain.TAG);
        if(fragmentMain == null){
            ScreenManager.getInst().openFragmentWithAnimation(getSupportFragmentManager(),R.id.content_frame,
                    new FragmentMain(),true,FragmentMain.TAG);
        }else {
            if(fragmentMain.isVisible()){
                mDrawerLayout.closeDrawer(GravityCompat.START);
            }else {
                for(int i = 0; i< getSupportFragmentManager().getBackStackEntryCount();i++){
                    getSupportFragmentManager().popBackStack();
                }
                showFragmentMain();
            }
        }
    }

    private void openFragmentFavorite() {
        Fragment fragmentFavorite = getSupportFragmentManager().findFragmentByTag(FragmentFavorite.TAG);
        if(fragmentFavorite == null || !fragmentFavorite.isVisible()){
            Fragment fragmentMain = getSupportFragmentManager().findFragmentByTag(FragmentMain.TAG);
            if(fragmentMain == null || !fragmentMain.isVisible()){
                ScreenManager.getInst().openFragmentWithAnimation(getSupportFragmentManager(),R.id.content_frame,new FragmentFavorite(),false,FragmentFavorite.TAG);
            }else {
                ScreenManager.getInst().openFragmentWithAnimation(getSupportFragmentManager(),R.id.content_frame,new FragmentFavorite(),true,FragmentFavorite.TAG);

            }
        }else {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
        setupAlarm(myPreferences.getIntValue(Constant.KEY_SAVE_HOUR_ALARM,8),myPreferences.getIntValue(Constant.KEY_SAVE_MINUTE_ALARM,0)
                ,Constant.MORNING_REQUEST_CODE);
    }

    private void setupAlarm(int selectedHour,int selectedMinute,int requestCode) {
        Calendar calendar=Calendar.getInstance();
        int currentHour=calendar.get(Calendar.HOUR_OF_DAY);
        int currentMinutes=calendar.get(Calendar.MINUTE);
        int currentDay=calendar.get(Calendar.DAY_OF_MONTH);
        calendar.set(Calendar.HOUR_OF_DAY, selectedHour);
        calendar.set(Calendar.MINUTE, selectedMinute);
        if(selectedHour<currentHour ||(selectedHour==currentHour && selectedMinute< currentMinutes)) {
            calendar.set(Calendar.DAY_OF_MONTH,currentDay+1);
        }

        Intent alarmIntent = new Intent(MainActivity.this, AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(MainActivity.this, requestCode,
                alarmIntent, 0);
        mAlarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
    }

}

package com.example.nguyen.inspirationquotewithgame.database;

import java.util.List;

import io.realm.Realm;

/**
 * Created by Administrator on 14/04/2018.
 */

public class DatabaseManager {
    private static DatabaseManager instance;
    private Realm realm;

    private DatabaseManager() {
        realm=Realm.getDefaultInstance();
    }

    public static DatabaseManager getInstance() {
        if (instance == null) {
            instance = new DatabaseManager();
        }
        return instance;
    }

    public void saveQuote(QuoteInDatabase quoteInDatabase){
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(quoteInDatabase);
        realm.commitTransaction();
    }

    public QuoteInDatabase getQuoteByID(int id){
        return realm.where(QuoteInDatabase.class).equalTo("id",id).findFirst();
    }

    public List<QuoteInDatabase> getAllQuote(){
        return realm.where(QuoteInDatabase.class).findAll();
    }
}

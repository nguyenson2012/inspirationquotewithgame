package com.example.nguyen.inspirationquotewithgame.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Administrator on 28/01/2018.
 */

public class MyPreferences {
    private static MyPreferences instance;
    private static SharedPreferences sharedPreferences;

    public MyPreferences(Context activity) {
        sharedPreferences=activity.getSharedPreferences(Constant.MY_PREF, Context.MODE_PRIVATE);
    }

    public static MyPreferences getInstance(Activity activity){
        if(instance==null){
            instance=new MyPreferences(activity);
        }
        return instance;
    }

    public void saveIntValue(String key,int value){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(key,value);
        editor.apply();
    }

    public int getIntValue(String key,int defaultValue){
        return sharedPreferences.getInt(key,defaultValue);
    }

    public void saveBooleanValue(String key,boolean value){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key,value);
        editor.apply();
    }

    public boolean getBooleanValue(String key){
        return sharedPreferences.getBoolean(key,false);
    }

    public void saveStringValue(String key, String value){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key,value);
        editor.apply();
    }

    public String getStringValue(String key){
        return sharedPreferences.getString(key,"");
    }
}

package com.example.nguyen.inspirationquotewithgame.fragment;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.nguyen.inspirationquotewithgame.R;
import com.example.nguyen.inspirationquotewithgame.database.DatabaseManager;
import com.example.nguyen.inspirationquotewithgame.database.QuoteInDatabase;
import com.example.nguyen.inspirationquotewithgame.utils.Constant;
import com.example.nguyen.inspirationquotewithgame.utils.MyPreferences;
import com.example.nguyen.inspirationquotewithgame.utils.ScreenManager;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by prdcv172 on 4/23/18.
 */

public class FragmentResult extends Fragment implements View.OnClickListener{
    public static final String TAG = "FragmentResult";
    private TextView mTextviewMonthScore;
    private TextView mTextviewAllQuoteDone;
    private Button mButtonReplay;
    private Button mButtonShare;
    private TextView mTextviewShareResultTogetGift;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_result,container,false);
        setupView(rootView);
        blinkTextViewShareResult();
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        showQuotePlayedThisTime();
        showMonthScore();
        registerEvent();
    }

    private void registerEvent() {
        mButtonReplay.setOnClickListener(this);
        mButtonShare.setOnClickListener(this);
    }

    private void showMonthScore() {
        int monthScore = MyPreferences.getInstance(getActivity()).getIntValue(Constant.KEY_SAVE_MONTH_SCORE,0);
        mTextviewMonthScore.setText(monthScore+"");
    }

    private void showQuotePlayedThisTime() {
        StringBuilder combineQuotePlayed = new StringBuilder("");
        String combineQuoteID = MyPreferences.getInstance(getActivity()).getStringValue(Constant.KEY_SAVE_COMBINE_PLAYED_QUOTE_ID);
        String[] playedQuoteIDs = combineQuoteID.split("-");
        for(String quoteID : playedQuoteIDs){
            if(quoteID.length() != 0){
                QuoteInDatabase quoteInDatabase = DatabaseManager.getInstance().getQuoteByID(Integer.parseInt(quoteID));
                if(quoteInDatabase!=null){
                    combineQuotePlayed.append("\n").append(quoteInDatabase.getQuote());
                }
            }
        }
        mTextviewAllQuoteDone.setText(combineQuotePlayed.toString());
    }

    private void setupView(View rootView) {
        mTextviewMonthScore = rootView.findViewById(R.id.result_tv_score_this_month);
        mTextviewAllQuoteDone = rootView.findViewById(R.id.result_tv_all_quote_done);
        mButtonReplay = rootView.findViewById(R.id.result_button_replay);
        mButtonShare = rootView.findViewById(R.id.result_button_share);
        mTextviewShareResultTogetGift = rootView.findViewById(R.id.result_tv_share_result_to_get_gift);
    }

    private void blinkTextViewShareResult(){
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                int timeToBlink = 500;    //in milissegunds
                try{Thread.sleep(timeToBlink);}catch (Exception e) {}
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if(mTextviewShareResultTogetGift.getVisibility() == View.VISIBLE){
                            mTextviewShareResultTogetGift.setVisibility(View.INVISIBLE);
                        }else{
                            mTextviewShareResultTogetGift.setVisibility(View.VISIBLE);
                        }
                        blinkTextViewShareResult();
                    }
                });
            }
        }).start();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.result_button_replay:
                ScreenManager.getInst().openFragmentWithAnimation(getActivity().getSupportFragmentManager(),R.id.content_frame,new FragmentPlay(),
                        false,FragmentPlay.TAG);
                break;
            case R.id.result_button_share:
                takeScreenShortAndShare();
                break;
        }
    }

    private void takeScreenShortAndShare() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder builder;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        builder = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Material_Dialog_Alert);
                    } else {
                        builder = new AlertDialog.Builder(getActivity());
                    }
                    builder.setTitle(getString(R.string.information))
                            .setMessage(getString(R.string.need_save_image))
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();

                } else {
                    requestPermissions(
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            Constant.MY_PERMISSION_REQUEST_WRITE_EXTERNAL);
                }
            }else {
                captureViewAndShare();
            }
        }else {
            captureViewAndShare();
        }
    }

    private void captureViewAndShare(){
        View rootView = getActivity().getWindow().getDecorView().getRootView();
        rootView.setDrawingCacheEnabled(true);
        Bitmap bitmap = Bitmap.createBitmap(rootView.getDrawingCache());
        rootView.setDrawingCacheEnabled(false);
        new ScreenShot().execute(bitmap);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Constant.MY_PERMISSION_REQUEST_WRITE_EXTERNAL: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    captureViewAndShare();
                } else {
                    shareApp();
                }
            }
        }
    }

    class ScreenShot extends AsyncTask<Bitmap, String, String> {
        File imageFile;
        @Override
        protected String doInBackground(Bitmap... bitmaps) {
            int count;
            try {
                // image naming and path  to include sd card  appending name you choose for file
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
                String currentDateandTime = sdf.format(Calendar.getInstance().getTime());
                String mPath = Environment.getExternalStorageDirectory().toString() + "/" +"freaking_english"+currentDateandTime+".png";
                imageFile = new File(mPath);

                FileOutputStream outputStream = new FileOutputStream(imageFile);
                int quality = 100;
                bitmaps[0].compress(Bitmap.CompressFormat.PNG, quality, outputStream);
                outputStream.flush();
                outputStream.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }
        @Override
        protected void onPostExecute(String file_url) {
            File checkFile=new File(imageFile.getAbsolutePath());
            if(checkFile.exists()) {
                shareResult(imageFile);
            }else {
                shareApp();
            }
        }
    }

    private void shareResult(File imageFile) {
        List<Intent> shareIntentsLists = new ArrayList<Intent>();
        Intent shareIntent = new Intent();
        Uri photoURI = FileProvider.getUriForFile(getActivity(),
                getActivity().getApplicationContext().getPackageName() + ".com.example.nguyen.inspirationquotewithgame.provider",
                new File(imageFile.getAbsolutePath()));
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.share_img_message));
        shareIntent.putExtra(Intent.EXTRA_STREAM, photoURI);
        shareIntent.setType("image/*");
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        List<ResolveInfo> resInfos = getActivity().getPackageManager().queryIntentActivities(shareIntent, 0);
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        Intent chooserIntent = Intent.createChooser(shareIntent, getString(R.string.choose_app_to_share));
        startActivity(chooserIntent);
    }

    private void shareApp() {
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("text/plain");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            share.addFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT);
        }
        share.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.share_app_title));
        share.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.link_google_play_for_share));

        startActivity(Intent.createChooser(share, getResources().getString(R.string.share_app_title)));
    }
}

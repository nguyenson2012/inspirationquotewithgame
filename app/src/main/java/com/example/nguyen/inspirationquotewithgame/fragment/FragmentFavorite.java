package com.example.nguyen.inspirationquotewithgame.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.nguyen.inspirationquotewithgame.R;
import com.example.nguyen.inspirationquotewithgame.adapter.PagerQuoteFavorite;
import com.example.nguyen.inspirationquotewithgame.database.DatabaseManager;
import com.example.nguyen.inspirationquotewithgame.database.QuoteInDatabase;
import com.example.nguyen.inspirationquotewithgame.model.InspirationQuote;
import com.example.nguyen.inspirationquotewithgame.utils.Constant;
import com.example.nguyen.inspirationquotewithgame.utils.MyPreferences;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 14/04/2018.
 */

public class FragmentFavorite extends Fragment {
    public static final String TAG = "FragmentFavorite";
    private ViewPager mViewPager;
    private ImageView mImageArrowLeft;
    private ImageView mImageArrowRight;
    private List<InspirationQuote> mListFavoriteQuote;

    private PagerQuoteFavorite mPagerQuoteAdapter;

    private MyPreferences myPreferences;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main,container,false);
        mViewPager = rootView.findViewById(R.id.main_view_pager_quote);
        mImageArrowLeft = rootView.findViewById(R.id.main_image_arrow_left);
        mImageArrowRight = rootView.findViewById(R.id.main_image_arrow_right);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        myPreferences = MyPreferences.getInstance(getActivity());
        mListFavoriteQuote = restoreFavoriteQuote();
        mPagerQuoteAdapter = new PagerQuoteFavorite(mListFavoriteQuote,getActivity());
        mViewPager.setAdapter(mPagerQuoteAdapter);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if(position == 0){
                    mImageArrowLeft.setVisibility(View.INVISIBLE);
                    mImageArrowRight.setVisibility(View.VISIBLE);
                }else if(position == mListFavoriteQuote.size() -1){
                    mImageArrowLeft.setVisibility(View.VISIBLE);
                    mImageArrowRight.setVisibility(View.INVISIBLE);
                }else {
                    mImageArrowLeft.setVisibility(View.VISIBLE);
                    mImageArrowRight.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private List<InspirationQuote> restoreFavoriteQuote() {
        List<InspirationQuote> favoriteQuotes = new ArrayList<>();

        List<Integer> quoteIDs = new ArrayList<>();
        String combineFavoriteQuoteID = myPreferences.getStringValue(Constant.KEY_SAVE_COMBINE_FAVORITE_QUOTE_ID);
        String[] arrayQuoteID = combineFavoriteQuoteID.split("-");
        if(arrayQuoteID.length != 0 && combineFavoriteQuoteID.length()!=0){
            for(String quoteID : arrayQuoteID){
                if(quoteID.length()!=0){
                    quoteIDs.add(Integer.parseInt(quoteID));
                }
            }
        }
        if(quoteIDs.size() != 0){
            for(Integer quoteID : quoteIDs) {
                QuoteInDatabase quoteInDatabase = DatabaseManager.getInstance().getQuoteByID(quoteID);
                InspirationQuote inspirationQuote = new InspirationQuote(quoteInDatabase.getId(),quoteInDatabase.getQuote(),quoteInDatabase.getAuthor());
                favoriteQuotes.add(inspirationQuote);
            }
        }
        return favoriteQuotes;
    }
}

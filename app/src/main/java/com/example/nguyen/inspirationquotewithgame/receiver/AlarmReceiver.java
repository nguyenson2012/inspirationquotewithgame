package com.example.nguyen.inspirationquotewithgame.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.nguyen.inspirationquotewithgame.MainActivity;
import com.example.nguyen.inspirationquotewithgame.service.AlarmServiceBroadcastReciever;
import com.example.nguyen.inspirationquotewithgame.utils.Constant;
import com.example.nguyen.inspirationquotewithgame.utils.MyPreferences;
import com.example.nguyen.inspirationquotewithgame.utils.StaticWakeLock;

/**
 * Created by Administrator on 17/09/2017.
 */

public class AlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent mathAlarmServiceIntent = new Intent(
                context, AlarmServiceBroadcastReciever.class);
        context.sendBroadcast(mathAlarmServiceIntent, null);

        StaticWakeLock.lockOn(context);

        Intent i = new Intent(context, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        MyPreferences myPreference = new MyPreferences(context);
        if(myPreference.getBooleanValue(Constant.IS_ALARM_AVAILABLE)){
            context.startActivity(i);
        }

    }
}

package com.example.nguyen.inspirationquotewithgame.database;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Administrator on 14/04/2018.
 */

public class QuoteInDatabase extends RealmObject{
    @PrimaryKey
    private int id;

    private String quote;
    private String author;
    private boolean isOwnQuote;

    public QuoteInDatabase() {
    }

    public QuoteInDatabase(int id, String quote, String author, boolean isOwnQuote) {
        this.id = id;
        this.quote = quote;
        this.author = author;
        this.isOwnQuote = isOwnQuote;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQuote() {
        return quote;
    }

    public void setQuote(String quote) {
        this.quote = quote;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public boolean isOwnQuote() {
        return isOwnQuote;
    }

    public void setOwnQuote(boolean ownQuote) {
        isOwnQuote = ownQuote;
    }
}

package com.example.nguyen.inspirationquotewithgame.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.nguyen.inspirationquotewithgame.R;
import com.example.nguyen.inspirationquotewithgame.adapter.PagerQuoteAdapter;
import com.example.nguyen.inspirationquotewithgame.database.DatabaseManager;
import com.example.nguyen.inspirationquotewithgame.database.QuoteInDatabase;
import com.example.nguyen.inspirationquotewithgame.model.InspirationQuote;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Administrator on 14/04/2018.
 */

public class FragmentMain extends Fragment {
    public static final String TAG = "FragmentMain";
    private ViewPager mViewPager;
    private ImageView mImageArrowLeft;
    private ImageView mImageArrowRight;
    private List<InspirationQuote> mListQuote;

    private PagerQuoteAdapter mPagerQuoteAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main,container,false);
        setupView(rootView);
        return rootView;
    }

    private void setupView(View rootView) {
        mViewPager = rootView.findViewById(R.id.main_view_pager_quote);
        mImageArrowLeft = rootView.findViewById(R.id.main_image_arrow_left);
        mImageArrowRight = rootView.findViewById(R.id.main_image_arrow_right);
    }

    public List<InspirationQuote> getExampleQuote(){
        List<InspirationQuote> listQuote = new ArrayList<>();
        for(int i = 1;i<10;i++){
            InspirationQuote inspirationQuote = new InspirationQuote(i,"Quote number "+i,"Unknown");
            listQuote.add(inspirationQuote);
        }
        return listQuote;
    }

    public List<InspirationQuote> getQuoteFromDatabase(){
        List<InspirationQuote> listQuote = new ArrayList<>();
        List<InspirationQuote> randomQuote = new ArrayList<>();
        List<QuoteInDatabase> allQuote = DatabaseManager.getInstance().getAllQuote();
        for(QuoteInDatabase quoteInDatabase : allQuote){
            InspirationQuote inspirationQuote = new InspirationQuote(quoteInDatabase.getId(),quoteInDatabase.getQuote(),quoteInDatabase.getAuthor());
            listQuote.add(inspirationQuote);
        }
        Random random = new Random();
        do{
            int randomPosition = random.nextInt(listQuote.size());
            randomQuote.add(listQuote.get(randomPosition));
            listQuote.remove(randomPosition);
        }while (randomQuote.size() != allQuote.size());
        return randomQuote;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mListQuote = getQuoteFromDatabase();
        mPagerQuoteAdapter = new PagerQuoteAdapter(mListQuote,getActivity());
        mViewPager.setAdapter(mPagerQuoteAdapter);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if(position == 0){
                    mImageArrowLeft.setVisibility(View.INVISIBLE);
                    mImageArrowRight.setVisibility(View.VISIBLE);
                }else if(position == mListQuote.size() -1){
                    mImageArrowLeft.setVisibility(View.VISIBLE);
                    mImageArrowRight.setVisibility(View.INVISIBLE);
                }else {
                    mImageArrowLeft.setVisibility(View.VISIBLE);
                    mImageArrowRight.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }
}

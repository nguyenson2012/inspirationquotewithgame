package com.example.nguyen.inspirationquotewithgame.model;

/**
 * Created by Administrator on 14/04/2018.
 */

public class InspirationQuote {
    private int id;
    private String quote;
    private String author;

    public InspirationQuote() {
    }

    public InspirationQuote(int id, String quote, String author) {
        this.id = id;
        this.quote = quote;
        this.author = author;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQuote() {
        return quote;
    }

    public void setQuote(String quote) {
        this.quote = quote;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}

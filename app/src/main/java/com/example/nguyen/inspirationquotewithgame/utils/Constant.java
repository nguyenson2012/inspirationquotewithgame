package com.example.nguyen.inspirationquotewithgame.utils;

/**
 * Created by Administrator on 28/01/2018.
 */

public class Constant {

    public static final String MY_PREF = "my_pref";
    public static final String KEY_SAVE_DONE_SAVE_DATABASE = "KEY_SAVE_DONE_SAVE_DATABASE";
    public static final String KEY_SAVE_NUMBER_LAUNCH_APP = "KEY_SAVE_NUMBER_LAUNCH_APP";
    public static final String KEY_SAVE_COMBINE_FAVORITE_QUOTE_ID = "KEY_SAVE_COMBINE_FAVORITE_QUOTE_ID";
    public static final String KEY_DONE_RATE_APP = "KEY_DONE_RATE_APP";
    public static final int OWN_QUOTE_ID_DEFAULT = 5000;
    public static final String KEY_SAVE_LATEST_OWN_QUOTE_ID = "KEY_SAVE_LATEST_OWN_QUOTE_ID";
    public static final int TOTAL_TIME_FOR_CHALLENGE = 1*60*1000;
    public static final int TIME_INTERVAL = 1000;
    public static final String KEY_SAVE_MONTH_SCORE = "KEY_SAVE_MONTH_SCORE";
    public static final String KEY_SAVE_COMBINE_PLAYED_QUOTE_ID = "KEY_SAVE_COMBINE_PLAYED_QUOTE_ID";
    public static final int MY_PERMISSION_REQUEST_WRITE_EXTERNAL = 2000;
    public static final String KEY_SAVE_HOUR_ALARM = "KEY_SAVE_HOUR_ALARM";
    public static final String KEY_SAVE_MINUTE_ALARM = "KEY_SAVE_MINUTE_ALARM";
    public static final int MORNING_REQUEST_CODE = 0;
    public static final String IS_ALARM_AVAILABLE = "IS_ALARM_AVAILABLE";
}

package com.example.nguyen.inspirationquotewithgame.model;

import java.util.List;

/**
 * Created by Administrator on 14/04/2018.
 */

public class AllQuote {
    private List<InspirationQuote> quotes;

    public AllQuote() {
    }

    public AllQuote(List<InspirationQuote> quotes) {
        this.quotes = quotes;
    }

    public List<InspirationQuote> getQuotes() {
        return quotes;
    }

    public void setQuotes(List<InspirationQuote> quotes) {
        this.quotes = quotes;
    }
}

package com.example.nguyen.inspirationquotewithgame.fragment;

import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;

import com.example.nguyen.inspirationquotewithgame.R;
import com.example.nguyen.inspirationquotewithgame.utils.CommonUtils;
import com.example.nguyen.inspirationquotewithgame.utils.Constant;
import com.example.nguyen.inspirationquotewithgame.utils.MyPreferences;
import com.suke.widget.SwitchButton;

import java.util.Calendar;

/**
 * Created by Administrator on 16/04/2018.
 */

public class FragmentNotification extends Fragment implements View.OnClickListener{
    public static final String TAG = "FragmentNotification";
    private TextView mTextviewReminderStatus;
    private TextView mTextviewReminderTime;
    private SwitchButton mSwitchReminder;
    private ImageView mImageChooseTimeReminder;

    private MyPreferences myPreferences;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_notification,container,false);
        setupView(rootView);
        registerEvent();
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        myPreferences = MyPreferences.getInstance(getActivity());
        mTextviewReminderTime.setText(getString(R.string.reminder_time)+" "+ CommonUtils.convertToTwoDigitNumber(8) + ":"
                +CommonUtils.convertToTwoDigitNumber(0));
    }

    private void registerEvent() {
        mTextviewReminderTime.setOnClickListener(this);
        mImageChooseTimeReminder.setOnClickListener(this);
        mSwitchReminder.setOnClickListener(this);
    }

    private void setupView(View rootView) {
        mTextviewReminderStatus = rootView.findViewById(R.id.notification_textview_reminder_status);
        mTextviewReminderTime = rootView.findViewById(R.id.notification_tv_time_alarm);
        mSwitchReminder = rootView.findViewById(R.id.notification_switch_alarm);
        mImageChooseTimeReminder = rootView.findViewById(R.id.notification_icon_choose_time_reminder);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.notification_switch_alarm:
                changeReminderStatus();
                break;
            case R.id.notification_tv_time_alarm:
                showDialogChooseReminderTime();
                break;
            case R.id.notification_icon_choose_time_reminder:
                showDialogChooseReminderTime();
                break;
        }
    }

    private void changeReminderStatus() {
        if(mSwitchReminder.isChecked()){
            mTextviewReminderTime.setVisibility(View.VISIBLE);
            mImageChooseTimeReminder.setVisibility(View.VISIBLE);
            mTextviewReminderStatus.setText(getString(R.string.reminder_is_on));
            myPreferences.saveBooleanValue(Constant.IS_ALARM_AVAILABLE,true);
        }else {
            mTextviewReminderTime.setVisibility(View.INVISIBLE);
            mImageChooseTimeReminder.setVisibility(View.INVISIBLE);
            mTextviewReminderStatus.setText(getString(R.string.reminder_is_off));
            myPreferences.saveBooleanValue(Constant.IS_ALARM_AVAILABLE,false);
        }
    }

    private void showDialogChooseReminderTime() {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                mTextviewReminderTime.setText(getString(R.string.reminder_time)+" "+ CommonUtils.convertToTwoDigitNumber(selectedHour) + ":"
                        +CommonUtils.convertToTwoDigitNumber(selectedMinute));
                myPreferences.saveIntValue(Constant.KEY_SAVE_HOUR_ALARM,selectedHour);
                myPreferences.saveIntValue(Constant.KEY_SAVE_MINUTE_ALARM,selectedMinute);
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.setTitle("Select Reminder Time");
        mTimePicker.show();
    }
}

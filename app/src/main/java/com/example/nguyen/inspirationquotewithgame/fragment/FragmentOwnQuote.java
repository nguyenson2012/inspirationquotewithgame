package com.example.nguyen.inspirationquotewithgame.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.nguyen.inspirationquotewithgame.R;
import com.example.nguyen.inspirationquotewithgame.database.DatabaseManager;
import com.example.nguyen.inspirationquotewithgame.database.QuoteInDatabase;
import com.example.nguyen.inspirationquotewithgame.utils.Constant;
import com.example.nguyen.inspirationquotewithgame.utils.MyPreferences;

/**
 * Created by nguyen on 22/04/2018.
 */

public class FragmentOwnQuote extends Fragment {
    private EditText mEdittextQuote;
    private EditText mEdittextAuthor;
    private Button mButtonAddOwnQuote;
    public static final String TAG = "FragmentOwnQuote";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_own_quote,container,false);
        setupView(rootView);
        return rootView;
    }

    private void setupView(View rootView) {
        mEdittextQuote = rootView.findViewById(R.id.own_edittext_quote_content);
        mEdittextAuthor = rootView.findViewById(R.id.own_edittext_author_content);
        mButtonAddOwnQuote = rootView.findViewById(R.id.own_button_add_own_quote);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mButtonAddOwnQuote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String quote = mEdittextQuote.getText().toString();
                String author = mEdittextAuthor.getText().toString();
                if(quote.length() == 0 || author.length() == 0){
                    Toast.makeText(getActivity(),getString(R.string.need_fill_enough_info),Toast.LENGTH_SHORT).show();
                }else {
                    int quoteID = MyPreferences.getInstance(getActivity()).getIntValue(Constant.KEY_SAVE_LATEST_OWN_QUOTE_ID,Constant.OWN_QUOTE_ID_DEFAULT);
                    MyPreferences.getInstance(getActivity()).saveIntValue(Constant.KEY_SAVE_LATEST_OWN_QUOTE_ID,quoteID+1);
                    QuoteInDatabase quoteInDatabase = new QuoteInDatabase(quoteID+1,quote,author,true);
                    DatabaseManager.getInstance().saveQuote(quoteInDatabase);
                    Toast.makeText(getActivity(),getString(R.string.done_save_own_quote),Toast.LENGTH_SHORT).show();
                    resetField();
                }
            }
        });
    }

    private void resetField() {
        mEdittextAuthor.setText("");
        mEdittextQuote.setText("");
    }
}

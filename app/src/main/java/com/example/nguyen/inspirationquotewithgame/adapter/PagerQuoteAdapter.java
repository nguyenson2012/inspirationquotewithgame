package com.example.nguyen.inspirationquotewithgame.adapter;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.nguyen.inspirationquotewithgame.R;
import com.example.nguyen.inspirationquotewithgame.model.InspirationQuote;
import com.example.nguyen.inspirationquotewithgame.utils.Constant;
import com.example.nguyen.inspirationquotewithgame.utils.MyPreferences;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 14/04/2018.
 */

public class PagerQuoteAdapter extends PagerAdapter {
    private List<InspirationQuote> mListQuote;
    private Context mContext;
    private MyPreferences myPreferences;
    private List<Integer> listQFavoriteQuoteID;
    private boolean isFavoriteQuote;

    private static final DecelerateInterpolator DECCELERATE_INTERPOLATOR = new DecelerateInterpolator();
    private static final AccelerateInterpolator ACCELERATE_INTERPOLATOR = new AccelerateInterpolator();
    private static final OvershootInterpolator OVERSHOOT_INTERPOLATOR = new OvershootInterpolator(4);

    public PagerQuoteAdapter(List<InspirationQuote> mListQuote, Activity mContext) {
        this.mListQuote = mListQuote;
        this.mContext = mContext;
        myPreferences = MyPreferences.getInstance(mContext);
        listQFavoriteQuoteID = new ArrayList<>();
        listQFavoriteQuoteID = restoreFavoriteQuoteID();
    }

    private List<Integer> restoreFavoriteQuoteID() {
        List<Integer> quoteIDs = new ArrayList<>();
        String combineFavoriteQuoteID = myPreferences.getStringValue(Constant.KEY_SAVE_COMBINE_FAVORITE_QUOTE_ID);
        String[] arrayQuoteID = combineFavoriteQuoteID.split("-");
        if(arrayQuoteID.length != 0 && combineFavoriteQuoteID.length()!=0){
            for(String quoteID : arrayQuoteID){
                if(quoteID.length()!=0){
                    quoteIDs.add(Integer.parseInt(quoteID));
                }
            }
        }
        return quoteIDs;
    }

    @Override
    public int getCount() {
        return mListQuote.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.pager_quote, container, false);
        container.addView(layout);

        TextView mTextviewQuote = layout.findViewById(R.id.pager_tv_quote);
        final ImageView mImageViewFavorite = layout.findViewById(R.id.pager_image_favorite);
        ImageView mImageShare = layout.findViewById(R.id.pager_image_share);

        final InspirationQuote inspirationQuote = mListQuote.get(position);
        isFavoriteQuote = listQFavoriteQuoteID.contains(inspirationQuote);

        mTextviewQuote.setText(inspirationQuote.getQuote()+"\n"+"-"+inspirationQuote.getAuthor()+"-");
        if(listQFavoriteQuoteID.contains(inspirationQuote.getId())){
            mImageViewFavorite.setImageResource(R.drawable.heart_fill_white);
        }else {
            mImageViewFavorite.setImageResource(R.drawable.heart_outline_white);
        }

        mImageViewFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isFavoriteQuote){
                    isFavoriteQuote = false;
                    animateFavoriteButton(mImageViewFavorite,false);
                    listQFavoriteQuoteID.remove(new Integer(inspirationQuote.getId()));
                    String combineFavoriteID = "";
                    for(Integer quoteID : listQFavoriteQuoteID){
                        combineFavoriteID+="-"+quoteID;
                    }
                    myPreferences.saveStringValue(Constant.KEY_SAVE_COMBINE_FAVORITE_QUOTE_ID,combineFavoriteID);

                }else {
                    isFavoriteQuote = true;
                    animateFavoriteButton(mImageViewFavorite,true);
                    listQFavoriteQuoteID.add(new Integer(inspirationQuote.getId()));
                    String combineFavoriteID = "";
                    for(Integer quoteID : listQFavoriteQuoteID){
                        combineFavoriteID+="-"+quoteID;
                    }
                    myPreferences.saveStringValue(Constant.KEY_SAVE_COMBINE_FAVORITE_QUOTE_ID,combineFavoriteID);
                }
            }
        });
        mImageShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareQuote(inspirationQuote.getQuote()+"\n-"+inspirationQuote.getAuthor()+"-");
            }
        });
        return layout;
    }

    private void shareQuote(String quoteAndAuthor){
        String shareBody = quoteAndAuthor+"\nCheck "+mContext.getString(R.string.app_name)+" in Google Play Store";
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Share Quote");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        mContext.startActivity(Intent.createChooser(sharingIntent, mContext.getString(R.string.share_using)));
    }

    private void animateFavoriteButton(final ImageView imageViewFavorite, final boolean isChooseFavorite){
        AnimatorSet animatorSet = new AnimatorSet();
        ObjectAnimator rotationAnim = ObjectAnimator.ofFloat(imageViewFavorite, "rotation", 0f, 360f);
        rotationAnim.setDuration(300);
        rotationAnim.setInterpolator(ACCELERATE_INTERPOLATOR);

        ObjectAnimator bounceAnimX = ObjectAnimator.ofFloat(imageViewFavorite, "scaleX", 0.2f, 1f);
        bounceAnimX.setDuration(300);
        bounceAnimX.setInterpolator(OVERSHOOT_INTERPOLATOR);

        ObjectAnimator bounceAnimY = ObjectAnimator.ofFloat(imageViewFavorite, "scaleY", 0.2f, 1f);
        bounceAnimY.setDuration(300);
        bounceAnimY.setInterpolator(OVERSHOOT_INTERPOLATOR);
        bounceAnimY.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                if(isChooseFavorite){
                    imageViewFavorite.setImageResource(R.drawable.heart_fill_white);
                }else {
                    imageViewFavorite.setImageResource(R.drawable.heart_outline_white);
                }

            }

            @Override
            public void onAnimationEnd(Animator animation) {

            }
        });

        animatorSet.play(bounceAnimX).with(bounceAnimY).after(rotationAnim);
        animatorSet.start();
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
}

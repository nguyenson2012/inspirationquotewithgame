package com.example.nguyen.inspirationquotewithgame;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.IntentCompat;
import android.view.View;
import android.widget.ProgressBar;

import com.example.nguyen.inspirationquotewithgame.database.DatabaseManager;
import com.example.nguyen.inspirationquotewithgame.database.QuoteInDatabase;
import com.example.nguyen.inspirationquotewithgame.model.AllQuote;
import com.example.nguyen.inspirationquotewithgame.model.InspirationQuote;
import com.example.nguyen.inspirationquotewithgame.utils.Constant;
import com.example.nguyen.inspirationquotewithgame.utils.MyPreferences;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Created by Administrator on 14/04/2018.
 */

public class SplashActivity extends Activity {
    private ProgressBar mProgressLoadingData;

    private MyPreferences myPreferences;
    private DatabaseManager mDatabaseManager;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mProgressLoadingData = findViewById(R.id.splash_progress_bar_load_database);
        myPreferences = MyPreferences.getInstance(SplashActivity.this);
        mDatabaseManager =DatabaseManager.getInstance();
        myPreferences.saveIntValue(Constant.KEY_SAVE_NUMBER_LAUNCH_APP,myPreferences.getIntValue(Constant.KEY_SAVE_NUMBER_LAUNCH_APP,0) + 1);
        loadQuoteFromJsonFile();
        mProgressLoadingData.postDelayed(new Runnable() {
            @Override
            public void run() {
                showMainActivity();
            }
        },2000);
    }

    @SuppressLint("WrongConstant")
    private void showMainActivity() {
        Intent intent=new Intent(SplashActivity.this,MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.trans_in, R.anim.trans_out);
    }

    private void loadQuoteFromJsonFile(){
        String json = null;
        try {
            InputStream is = getAssets().open("inspiration_quote.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
            saveQuote2Databse(json);

            mProgressLoadingData.setVisibility(View.GONE);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void saveQuote2Databse(String jsonData) {
        Gson gson=new Gson();
        AllQuote allQuote=gson.fromJson(jsonData,AllQuote.class);
        if(allQuote!=null){
            if(allQuote.getQuotes()!=null){
                List<InspirationQuote> allWords=allQuote.getQuotes();
                myPreferences.saveBooleanValue(Constant.KEY_SAVE_DONE_SAVE_DATABASE,true);
                for(InspirationQuote quote:allWords){
                    QuoteInDatabase quoteInDatabase = new QuoteInDatabase(quote.getId(),quote.getQuote(),quote.getAuthor(),false);
                    mDatabaseManager.saveQuote(quoteInDatabase);
                }
            }
        }
    }
}
